"""
Revision
2019-06-04, Yingyi Tang, First Version
2019-06-05, Yingyi Tang, Catch export exception
2019-09-03, Yingyi Tang, Remove export job in ENM
"""
"""
Mandatory Parameter
--outdir: file output directory
--filename: output file name
--type: 3GPP or dynamic
--collection: user define collection
Optinal Parameter
--filter: filter file

Export CMD
cmedit export -f file:FilterFile --collection Collection --filetype 3GPP --jobname ExportJobName

python enm_bcgtool.py --outdir /home/shared/wxeric/enmca/standaloneca/enm_bcgtool/output --filename test --type 3GPP --collection rtm_test
"""

import getopt
import sys
import logging
import logging.config
import os
import enmscripting
import datetime
import time
import shutil

logger_conf = "/home/shared/gzyd_lky1/enmca/standaloneca/enm_bcgtool/conf/logger.conf"
log_file = "/home/shared/gzyd_lky1/enmca/standaloneca/enm_bcgtool/log/enm_bcgtool.log"
# default 120 minutes
EXPORT_TIMEOUT = 120

try:
    logging.config.fileConfig(logger_conf)
    logger = logging.getLogger("main")
except Exception, e:
    print(e)

def usage():
    str = \
        "NAME\n\
   enm_bcgtool.py - cmedit export\n\
DESCRIPTION\n\
   --outdir\n\
      Mandatory argument to specify output directory\n\
   --filename\n\
      Mandatory argument to specify output file name\n\
   --type\n\
      Mandatory argument to specify export file type 3GPP or dynamic\n\
   --collection\n\
      Mandatory argument to specify collection name\n\
   --filter\n\
      Optional argument to specify filter file"
    print(str)

def bcg():
    print("Export start")
    logger.info("Export start")
    # JobName
    now = datetime.datetime.now()
    jobTime = now.strftime("%Y%m%d%H%M%S")
    jobName = "enm_bcgtool" + jobTime

    #print sys.argv
    try:
        optNamesValuesList, restArgvs = getopt.getopt(sys.argv[1:], "", ["outdir=", "filename=", "type=", "collection=", "filter="])
    except Exception, e:
        print(e)
        logger.error(e)
        exit(1)
    #print(optNamesValuesList)
    #rint restArgvs

    outdir = None
    filename = None
    type = None
    collection = None
    filter = None

    for optName, optValue in optNamesValuesList:
        if optName == "--outdir":
            outdir = optValue
        elif optName == "--filename":
            filename = optValue
        elif optName == "--type":
            type = optValue
        elif optName == "--collection":
            collection = optValue
        elif optName == "--filter":
            filter = optValue
        else:
            pass

    # check outdir
    if not os.path.isdir(outdir) or not outdir:
        print("{dir} is not exist".format(dir=outdir))
        logger.error("{dir} is not exist".format(dir=outdir))
        exit(1)
    # no need check filename
    # check type
    if type not in ("3GPP", "dynamic"):
        print("type should be 3GPP or dynamic")
        logger.error("type should be 3GPP or dynamic")
        exit(1)
    # check collection
    if not collection:
        usage()
        logger.error("collection is mandatory")
        exit(1)
    else:
        cmd = "collection list"
        session = enmscripting.open()
        terminal = session.terminal()
        response = terminal.execute(cmd)
        code = response.http_response_code()
        output = response.get_output()
        collectionList = []
        if code == 200:
            #print(output)
            for line in output:
                if line.startswith("Name"):
                    collectionList.append(line.split(":")[1].strip())
            #print collectionList
            if collection not in collectionList:
                print("error. cannot find collection {col}".format(col=collection))
                logger.error("cannot find collection {col}".format(col=collection))
                enmscripting.close(session)
                exit(1)
        else:
            print("error. return code is {code}, return date is {date}".format(code=code, data=output))
            logger.error("return code is {code}, return date is {date}".format(code=code, data=output))
            enmscripting.close(session)
            exit(1)
        enmscripting.close(session)
    # check filter
    if filter and not os.path.isfile(filter):
        print("{file} file is not exist".format(file=filter))
        logger.error("{file} file is not exist".format(file=filter))
        exit(1)

    # cmd
    session = enmscripting.open()
    terminal = session.terminal()
    if filter:
        cmd = "cmedit export -f file:{ff} --collection {col} --filetype {ft} --jobname {jn}".format(ff=(os.path.basename(filter)),col=collection, ft=type, jn=jobName)
        with open(filter, "rb") as f:
            response = terminal.execute(cmd, f)
        print("read {f}".format(f=filter))
        logger.info("read {f}".format(f=filter))
    else:
        cmd = "cmedit export --collection {col} --filetype {ft} --jobname {jn}".format(col=collection, ft=type, jn=jobName)
        response = terminal.execute(cmd)
    print(cmd)
    logger.info(cmd)
    code = response.http_response_code()
    output = response.get_output()
    if code == 200:
        print output
        logger.info(output)
    else:
        print("error. return code is {code}, return date is {date}".format(code=code, data=output))
        logger.error("return code is {code}, return date is {date}".format(code=code, data=output))
        enmscripting.close(session)
        exit(1)
    enmscripting.close(session)

    # check export status
    checkTimes = 0
    cmd = "cmedit export --status --jobname {jn}".format(jn=jobName)
    session = enmscripting.open()
    terminal = session.terminal()
    try:
        while(checkTimes < EXPORT_TIMEOUT):
            response = terminal.execute(cmd)
            logger.info("{cmd}".format(cmd=cmd))
            code = response.http_response_code()
            output = response.get_output()
            print(output[2])
            logger.info(output[2])
            logger.info("status: {status}".format(status=output[2].split("\t")[2]))
            if code == 200:
                if output[2].split("\t")[2] == "STARTED":
                    print("Query job ok, job name is {jb}, job status is STARTED".format(jb=jobName))
                    logger.info("Query job ok, job name is {jb}, job status is STARTED".format(jb=jobName))
                    checkTimes += 1
                    if checkTimes == EXPORT_TIMEOUT:
                        print("Query job timeout")
                        logger.error("Query job timeout")
                        exit(1)
                elif "COMPLETED" in output[2].split("\t")[2]:
                    print("Query job ok, job name is {jb}, job status is {js}".format(jb=jobName, js=output[2].split("\t")[2]))
                    logger.info("Query job ok, job name is {jb}, job status is {js}".format(jb=jobName, js=output[2].split("\t")[2]))
                    break
                else:
                    print("Query job ok, job name is {jb}, BUT job status is {js}".format(jb=jobName, js=output[2].split("\t")[2]))
                    logger.error("Query job ok, job name is {jb}, BUT job status is {js}".format(jb=jobName, js=output[2].split("\t")[2]))
                    enmscripting.close(session)
                    exit(1)
                time.sleep(60)
            else:
                print("error. return code is {code}, return date is {date}".format(code=code, data=output))
                logger.error("return code is {code}, return date is {date}".format(code=code, data=output))
                enmscripting.close(session)
                exit(1)
    except Exception, e:
        print(e)
        logger.error(e)
        exit(1)
    finally:
        enmscripting.close(session)

    cmdOutFile = output[2].split("\t")[11]
    print("Export file {f}".format(f=cmdOutFile))
    logger.info("Export file {f}".format(f=cmdOutFile))
    #print("Export completed")
    enmscripting.close(session)

    outFile = os.path.join(outdir, filename)
    shutil.copy(cmdOutFile, outFile)
    print("copied to {of}".format(of=outFile))
    logger.info("copied to {of}".format(of=outFile))

    # Remove ENM export job
    cmd = "cmedit export --remove --jobname {jn}".format(jn=jobName)
    session = enmscripting.open()
    terminal = session.terminal()
    response = terminal.execute(cmd)
    #logger.info(response)
    output = response.get_output()
    if code == 200:
        logger.info(output)
    else:
        #print("error. return code is {code}, return date is {date}".format(code=code, data=output))
        logger.error("return code is {code}, return date is {date}".format(code=code, data=output))
        exit(1)
    enmscripting.close(session)

    logger.info("Export completed")

def clean():
    fsize = os.path.getsize(log_file) / 1024 / 2014
    logger.info("log file size is {size}M".format(size=fsize))
    if fsize >= 50:
        os.remove(log_file)
        logger.info("log file size is {size}M, removed {file}".format(size=fsize, file=log_file))
    else:
        pass

if __name__ == "__main__":
    bcg()
    clean()