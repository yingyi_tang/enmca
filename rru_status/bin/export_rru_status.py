# -*- coding: utf-8 -*-
import enmscripting
import globVar
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import os
import json
import logging
import logging.config
import datetime
import ConfigParser

try:
    sys.path.append(globVar.PARENT_DIR)
    from cmlib import CmController
except Exception, e:
    print(e)
    exit(1)

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("main")
except Exception, e:
    print(e)

OUTPUT_DIR = globVar.OUTPUT_DIR
SYSINI_FILE = globVar.SYSINI_FILE

def export():
    now = datetime.datetime.now()
    time = now.strftime("%Y%m%d%H%M") + "00"

    conf = ConfigParser.ConfigParser()
    try:
        conf.read(SYSINI_FILE)
        sysName = conf.get("system", "sysname")
        linkDirs = conf.get("link_directory", "dirs")
    except Exception, e:
        logger.error(e)
        exit(1)

    cmCon = CmController.CmController()
    code1, output1 = cmCon.exportRadioNodeRruStatus()
    code2, output2 = cmCon.exportErbsRruStatus()
    if code1 != 200:
        logger.error("return code1 {code}, output1 {output}".format(code=code1, output=output1))
        exit(1)
    elif code2 != 200:
        logger.error("return code2 {code}, output2 {output}".format(code=code2, output=output2))
        exit(1)
    else:
        outFileName = "rru_status_" + sysName + "_" + time + ".log"
        outFile = os.path.join(OUTPUT_DIR, outFileName)
        with open(outFile, "w") as f:
            f.write("\n".join(output1))
            f.write("\n")
            f.write("\n".join(output2))
        logger.info("generated {file}".format(file=outFile))
        linkDirsList = linkDirs.split(";")
        for linkDir in linkDirsList:
            if linkDir:
                if not os.path.isdir(linkDir):
                    logger.error("directory {dir} not exist".format(dir=linkDir))
                else:
                    linkFile = os.path.join(linkDir, outFileName)
                    os.symlink(outFile, linkFile)
                    logger.info("link {dstFile} to {srcFile}".format(dstFile=linkFile, srcFile=outFile))

if __name__ == "__main__":
    export()
