import enmscripting
import globVar
import sys
import os
import json
import logging
import logging.config
import datetime
import ConfigParser

try:
    sys.path.append(globVar.PARENT_DIR)
    from cmlib import CmController
    from comlib import CsvController
except Exception, e:
    print(e)
    exit(1)

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("main")
except Exception, e:
    print(e)

OUTPUT_DIR = globVar.OUTPUT_DIR
INPUT_DIR = globVar.INPUT_DIR
TEMP_DIR = globVar.TEMP_DIR

SYSINI_FILE = globVar.SYSINI_FILE

def export():
    # Date and Time
    now = datetime.datetime.now()
    time = now.strftime("%Y%m%d%H%M") + "00"
    # Load sys.ini
    conf = ConfigParser.ConfigParser()
    try:
        conf.read(SYSINI_FILE)
        sysName = conf.get("system", "sysname")
        linkDirs = conf.get("link_directory", "dirs")
    except Exception, e:
        logger.error(e)
        exit(1)
    # type
    typeList = ["FDD","NB","TDD"]
    #
    cmCon = CmController.CmController()
    for type in typeList:
        if type == "FDD":
            code, output = cmCon.exportFddCellStatus()
        elif type == "NB":
            code, output = cmCon.exportNbIotCellStatus()
        elif type == "TDD":
            code, output = cmCon.exportTddCellStatus()
        else:
            pass

        if code != 200:
            logger.error("return code {code}, output {output}".format(code=code, output=output))
            exit(1)
        elif output[0].startswith("Error"):
            logger.error("return output contain Error information: {output}".format(output=output))
            exit(1)
        else:
            outFileName = "cell_status_" + type + "_" + sysName + "_" + time + ".log"
            outFile = os.path.join(OUTPUT_DIR, outFileName)
            with open(outFile, "w") as f:
                f.write("\n".join(output))
            logger.info("generated {file}".format(file=outFile))
            linkDirsList = linkDirs.split(";")
            for linkDir in linkDirsList:
                if linkDir:
                    if not os.path.isdir(linkDir):
                        logger.error("directory {dir} not exist".format(dir=linkDir))
                    else:
                        linkFile = os.path.join(linkDir, outFileName)
                        os.symlink(outFile, linkFile)
                        logger.info("link {dstFile} to {srcFile}".format(dstFile=linkFile, srcFile=outFile))


if __name__ == "__main__":
    export()
