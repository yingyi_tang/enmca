import os
import json

version = "1.0.0.190920"
desc = "Add rru_status module"
moduleNameList = ["alarm_list", "cell_status", "node_fdn", "node_status", "rru_status",\
                  "cmlib", "comlib", "fmlib"]
newVersionDic = {}
newVersionDic["VERSION"] = version
newVersionDic["DESCRIPTION"] = desc

BASE_DIR = os.path.split(os.path.realpath(__file__))[0]
moduleDirList = []
for i in moduleNameList:
    moduleDirList.append(os.path.abspath(os.path.join(BASE_DIR, i)))
#print moduleDirList
moduleVersionFileList = []
for i in moduleDirList:
    moduleVersionFileList.append(os.path.join(i, "version.json"))
#print moduleVersionFileList

enmcaVersionFile = os.path.join(BASE_DIR, "version.json")

with open(enmcaVersionFile, "r") as f:
    origVersionContent = json.load(f)
    print origVersionContent

for file in moduleVersionFileList:
    with open(file, "r") as f:
        content = json.load(f)
    moduleName =  content.keys()[0]
    moduleVersion = content.values()[0][len(content.values()[0])-1]["version"]
    newVersionDic[moduleName] = moduleVersion
print newVersionDic

origVersionContent["enmca"].append(newVersionDic)
#print origVersionContent

with open(enmcaVersionFile, "w") as j:
    json.dump(origVersionContent, j, indent=1, sort_keys=True)