import ConfigParser
import logging
import logging.config
import globVar
import os
import commands

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("main")
except Exception, e:
    print(e)

SYSINI_FILE = globVar.SYSINI_FILE

def clean():
    conf = ConfigParser.ConfigParser()
    try:
        conf.read(SYSINI_FILE)
        linkDirs = conf.get("link_directory", "dirs")
    except Exception, e:
        logger.error(e)
        exit(1)
    # Clean output
    cmd = "/bin/find {dir}/ -mtime +3 -type f -exec rm {char} \;".format(dir=globVar.OUTPUT_DIR, char="{}")
    status, ret = commands.getstatusoutput(cmd)
    if status == 0:
        logger.info("ran {cmd}".format(cmd=cmd))
    else:
        logger.error("ran {cmd} failed".format(cmd=cmd))

    # Clean link directory
    linkDirsList = linkDirs.split(";")
    for linkDir in linkDirsList:
        if linkDir:
            if not os.path.isdir(linkDir):
                logger.error("directory {dir} not exist".format(dir=linkDir))
            else:
                cmd = "/bin/find {dir}/ -mtime +3 -type l -exec rm {char} \;".format(dir=linkDir, char="{}")
                status, ret = commands.getstatusoutput(cmd)
                if status == 0:
                    logger.info("ran {cmd}".format(cmd=cmd))
                else:
                    logger.error("ran {cmd} failed".format(cmd=cmd))

    # clean log file, 50M
    fsize = os.path.getsize(globVar.LOG_FILE) / 1024 / 2014
    if fsize >= 50:
        os.remove(globVar.LOG_FILE)
        logger.info("log file size is {size}M, removed {file}".format(size=fsize, file=globVar.LOG_FILE))
    else:
        pass

if __name__ == "__main__":
    clean()
