import datetime
import enmscripting
import globVar
import sys
import os
import logging
import logging.config
import datetime
import ConfigParser

try:
    sys.path.append(globVar.PARENT_DIR)
    from fmlib import FmController
except Exception, e:
    print(e)
    exit(1)

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("main")
except Exception, e:
    print(e)

OUTPUT_DIR = globVar.OUTPUT_DIR
SYSINI_FILE = globVar.SYSINI_FILE

DELTA = -15

def export():
    now = datetime.datetime.now()
    cur_date = now.strftime("%Y-%m-%d")
    end_time = now.strftime("%H:%M") + ":00"
    delta = datetime.timedelta(minutes=DELTA)
    start_time = (now + delta).strftime("%H:%M") + ":01"

    conf = ConfigParser.ConfigParser()
    try:
        conf.read(SYSINI_FILE)
        sysName = conf.get("system", "sysname")
        linkDirs = conf.get("link_directory", "dirs")
    except Exception, e:
        logger.error(e)
        exit(1)

    fmCon = FmController.FmController()
    stime = cur_date + "T" + start_time
    etime= cur_date + "T" + end_time
    logger.info("get alarms from {stime} to {etime}".format(stime=stime, etime=etime))
    code, output = fmCon.getHistAlarms(stime=stime, etime=etime)
    if code != 200:
        logger.error("return code {code}, output {output}".format(code=code, output=output))
        exit(1)
    else:
        outFileName = "alarm_history_" + sysName + "_" + now.strftime("%Y%m%d%H%M") + "00.log"
        outFile = os.path.join(OUTPUT_DIR, outFileName)
        with open(outFile, "w") as f:
            f.write("\n".join(output))
        logger.info("generated {file}".format(file=outFile))
        linkDirsList = linkDirs.split(";")
        for linkDir in linkDirsList:
            if linkDir:
                if not os.path.isdir(linkDir):
                    logger.error("directory {dir} not exist".format(dir=linkDir))
                else:
                    linkFile = os.path.join(linkDir, outFileName)
                    os.symlink(outFile, linkFile)
                    logger.info("link {dstFile} to {srcFile}".format(dstFile=linkFile, srcFile=outFile))

if __name__ == "__main__":
    export()
