#!/usr/bin/env python
import os

# MAIN DIR
# /home/nmsadm/cadev/cell_status
BASE_DIR = os.path.split(os.path.realpath(__file__))[0]

# Parent Dir
PARENT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", ".."))

# Directory
CONF_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "conf"))
LOG_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "log"))
INPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "input"))
OUTPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "output"))
TEMP_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "temp"))

# Files
LOG_FILE = os.path.abspath(os.path.join(LOG_DIR, "node_status.log"))
LOGGER_FILE = os.path.abspath(os.path.join(CONF_DIR, "logger.conf"))
SYSINI_FILE = os.path.abspath(os.path.join(CONF_DIR, "sys.ini"))

if __name__ == "__main__":
    print BASE_DIR
    print LOGGER_FILE