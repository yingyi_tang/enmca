import enmscripting
import globVar
import sys
import os
import json
import logging
import logging.config
import datetime
import ConfigParser

try:
    sys.path.append(globVar.PARENT_DIR)
    from cmlib import CmController
    from comlib import CsvController
except Exception, e:
    print(e)
    exit(1)

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("main")
except Exception, e:
    print(e)

OUTPUT_DIR = globVar.OUTPUT_DIR
INPUT_DIR = globVar.INPUT_DIR
TEMP_DIR = globVar.TEMP_DIR

SYSINI_FILE = globVar.SYSINI_FILE

def export():
    # Date and Time
    now = datetime.datetime.now()
    time = now.strftime("%Y%m%d%H%M") + "00"
    # Load sys.ini
    conf = ConfigParser.ConfigParser()
    try:
        conf.read(SYSINI_FILE)
        sysName = conf.get("system", "sysname")
        linkDirs = conf.get("link_directory", "dirs")
    except Exception, e:
        logger.error(e)
        exit(1)
    #
    cmCon = CmController.CmController()
    # detail node info
    code1, output1 = cmCon.exportDetailNode()
    code2, output2 = cmCon.exportComAndCppIp()
    code3, output3 = cmCon.exportAlarmStatusForRTMasCM()
    if code1 != 200:
        logger.error("return code {code}, output {output}".format(code=code1, output=output1))
    elif code2 != 200:
        logger.error("return code {code}, output {output}".format(code=code2, output=output2))
    elif code3 != 200:
        logger.error("return code {code}, output {output}".format(code=code3, output=output3))
    elif output1[0].startswith("Error"):
        logger.error("return output contain Error information: {output}".format(output=output1))
    elif output2[0].startswith("Error"):
        logger.error("return output contain Error information: {output}".format(output=output2))
    elif output3[0].startswith("Error"):
        logger.error("return output contain Error information: {output}".format(output=output3))
    else:
        outFileName = "node_status_" + sysName + "_" + time + ".log"
        outFile = os.path.join(OUTPUT_DIR, outFileName)
        with open(outFile, "w") as f:
            f.write("\n".join(output1))
        with open(outFile, "a") as f:
            f.write("\n")
            f.write("\n".join(output2))
        with open(outFile, "a") as f:
            f.write("\n")
            f.write("\n".join(output3))
        logger.info("generated {file}".format(file=outFile))
        linkDirsList = linkDirs.split(";")
        for linkDir in linkDirsList:
            if linkDir:
                if not os.path.isdir(linkDir):
                    logger.error("directory {dir} not exist".format(dir=linkDir))
                else:
                    linkFile = os.path.join(linkDir, outFileName)
                    os.symlink(outFile, linkFile)
                    logger.info("link {dstFile} to {srcFile}".format(dstFile=linkFile, srcFile=outFile))

if __name__ == "__main__":
    export()
