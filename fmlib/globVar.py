#!/usr/bin/env python
import os

# MAIN DIR
BASE_DIR = os.path.split(os.path.realpath(__file__))[0]

# Parent Dir
PARENT_DIR = os.path.abspath(os.path.join(BASE_DIR, ".."))
