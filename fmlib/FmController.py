import enmscripting
import globVar
import sys

try:
    sys.path.append(globVar.PARENT_DIR)
    from comlib import CliController
except Exception, e:
    print(e)
    exit(1)

class FmController:
    def __init__(self):
        pass

    def getActiveAlarms(self):
        cmd = "alarm get * -v"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        return code, output

    def getHistAlarms(self, stime, etime):
        cmd = "alarm hist * -ta lastUpdated --begin {st} --end {et} -v".format(st=stime, et=etime)
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        return code, output
