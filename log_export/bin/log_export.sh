#!/bin/bash

curl=`which curl`
es_url='elasticsearch:9200/enm_logs*/_data'
utc_offset="+0800"
file_name=`date +%Y%m%d%H%M00`
system_name=yjenm1
ca_purpose=log

export_path=/ericsson/pmic1/enmca/log_export/output


function log_export()
{
    start_time=`date -d "5 min ago" +%Y-%m-%dT%H:%M:00`
    end_time=`date +%Y-%m-%dT%H:%M:00`
    ${curl} -G ${es_url} --data-urlencode "q=timestamp:["${start_time}${utc_offset}" TO "${end_time}${utc_offset}"] AND ((host:"amos" AND message:"AMOS.COMMAND") OR (host:"cmserv" AND message:"CM-EDITOR" NOT message:"ONGOING") OR host:"mscmce" OR host:"mscm")" --data-urlencode 'format=csv' > ${export_path}/${ca_purpose}_${system_name}_${file_name}.csv
    zip -m ${export_path}/${ca_purpose}_${system_name}_${file_name}.zip ${export_path}/${ca_purpose}_${system_name}_${file_name}.csv
}

function log_cleanup()
{
    find ${export_path} -type f -ctime +2 -name *.zip -exec rm {} \;
}

log_cleanup
log_export

