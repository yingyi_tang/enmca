
import enmscripting
import globVar
import os
import sys

try:
    sys.path.append(globVar.PARENT_DIR)
    from comlib import CliController
except Exception, e:
    print(e)
    exit(1)

class CmController:
    def __init__(self):
        pass

    def exportFddCellStatus(self):
        cmd = "cmedit get * EutranCELLFDD.(administrativeState,operationalState)"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        if code == 200:
            return code, output
        else:
            return code, "ERROR"

    def exportNbIotCellStatus(self):
        cmd = "cmedit get * NbIotCell.(administrativeState,operationalState)"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        if code == 200:
            return code, output
        else:
            return code, "ERROR"

    def exportTddCellStatus(self):
        cmd = "cmedit get * EutranCELLTDD.(administrativeState,operationalState)"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        if code == 200:
            return code, output
        else:
            return code, "ERROR"

    def exportNodeStatus(self):
        cmd = "cmedit get * CmFunction.syncStatus"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        if code == 200:
            return code, output
        else:
            return code, "ERROR"

    def exportNodeFdn(self):
        cmd = "cmedit get * MeContext"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        if code == 200:
            return code, output
        else:
            return code, "ERROR"

    def exportDetailNode(self):
        cmd = "cmedit get *E* --detailnode"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        if code == 200:
            return code, output
        else:
            return code, "ERROR"

    def exportComAndCppIp(self):
        cmd = "cmedit get * ComConnectivityInformation.ipAddress;CppConnectivityInformation.ipAddress"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        if code == 200:
            return code, output
        else:
            return code, "ERROR"

    def exportAlarmStatusForRTMasCM(self):
        cmd = "alarm status *"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        if code == 200:
            return code, output
        else:
            return code, "ERROR"

    def exportRadioNodeRruStatus(self):
        cmd = "cmedit get * FieldReplaceableUnit.(administrativeState,operationalState)"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        if code == 200:
            return code, output
        else:
            return code, "ERROR"

    def exportErbsRruStatus(self):
        cmd = "cmedit get * AuxPlugInUnit.(administrativeState,operationalState)"
        cliCon = CliController.CliController()
        code, output = cliCon.runCli(cmd)
        if code == 200:
            return code, output
        else:
            return code, "ERROR"
