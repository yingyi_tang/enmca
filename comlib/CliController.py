import enmscripting
import os

class CliController:
    def __init__(self):
        pass

    def runCli(self, cmd):
        session = enmscripting.open()
        terminal = session.terminal()
        response = terminal.execute(cmd)
        code = response.http_response_code()
        output = response.get_output()
        return code,output
        enmscripting.close(session)

    def runCliDownloadFiles(self, cmd, file):
        """
        downloaded filename will be file_<num>
        format will be csv
        :param cmd:
        :param file:
        :return:
        """
        session = enmscripting.open()
        terminal = session.terminal()
        response = terminal.execute(cmd)
        code = response.http_response_code()
        output = response.get_output()
        filesNum = len(response.files())
        num = 0
        if response.has_files():
            for enm_file in response.files():
                num += 1
                newFile = file + "_" + str(num)
                enm_file.download(path=newFile)
        return code, output, filesNum
        enmscripting.close(session)

if __name__ == "__main__":
    cliCon = CliController()
    cmd = "cmedit get * EutranCELLFD.(cellId,tac,administrativeState,operationalState) -t"
    code, output = cliCon.runCli(cmd)
    print code
    print output
