
import os
import csv

class CsvController:
    def __init__(self):
        pass

    def writeCsv(self, csvfile, line):
        if not os.path.isfile(csvfile):
            f = open(csvfile, "w")
            f.close()
        csvFile = file(csvfile, "ab")
        writter = csv.writer(csvFile)
        writter.writerow(line)
        csvFile.close()